import math
import sys
pin = sys.stdin
pout = sys.stdout

n = int(pin.readline())

for i in range(n):
	r, c = [float(xi) for xi in pin.readline().split()]
#	ans = c/(2*(1-(2*r/c)
	ans = c/2 * (1 + (2*r/c)**2) / (1 - (2*r/c)**2)
	print "%.3f" % round(ans, 3)
	
