#include<bits/stdc++.h>
using namespace std;

int main() {
    int rn;cin>>rn;
    while (rn-->0) {
        double r,c;cin>>r>>c;
        double ans = c/2 * (1 + 4*r*r/c/c) / (1 - 4*r*r/c/c);
        printf("%.3f\n", ans);
    }
}
