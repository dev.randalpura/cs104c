(TeX-add-style-hook
 "lecture"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("beamer" "10pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("algpseudocode" "noend")))
   (TeX-run-style-hooks
    "latex2e"
    "beamer"
    "beamer10"
    "algorithm"
    "algpseudocode"))
 :latex)

