\documentclass[10pt]{beamer}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}

\usetheme{Copenhagen}

\title{Union Find}
\author{Aaron Lamoreaux, Luke Gretta}
\institute{UT Austin}
\date{Fall 2021}

\begin{document}

\frame{\titlepage}

\begin{frame}
  \frametitle{Announcements}

  \pause
  \begin{itemize}
  \item Find the Bug \#10: Edges which are not connected to source / sink should
    have capacity $\infty$. The code makes all edges have capacity $1$. \pause
  \item Vanilla Problem Hurricane: Add a sink node which connects to all safe
    intersections. Compute maximum from $1$ to the sink. Check if this is at
    least the number of people. \pause
  \item Last UTPC contest will be December $3^{\text{rd}}$.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Motivating Problem: Social Network}

  Again, we want to build a social network app. We want to support the following
  operations: \pause
  \begin{itemize}
  \item \textsc{Add}: Add user \pause
  \item $\textsc{Union}(i, j)$: Connect users $i$ and $j$ \pause
  \item $\textsc{Connected}(i, j)$: Check if two users $i$ and $j$ are connected, possibly indirectly
  \end{itemize}
  
\end{frame}

\begin{frame}
  \frametitle{Flood Fill}

  We can already solve this problem using a graph. \pause
  \begin{itemize}
  \item \textsc{Add}: Add a new vertex to our graph. \pause
  \item $\textsc{Union}(i, j)$: Add an edge $e = (i, j)$ to our graph. \pause
  \item $\textsc{Connected}(i, j)$: Run $\textsc{DFS}$ from $i$ and check if $j$
    is reachable
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Flood Fill Runtime}

  This gives us the following runtimes: \pause
  \begin{itemize}
  \item \textsc{Add}: $O(1)$ \pause
  \item $\textsc{Union}(i, j)$: $O(1)$ \pause
  \item $\textsc{Connected}(i, j)$: $O(V + E)$ \pause
  \end{itemize}

  Can we do better than $O(V + E)$ though?
\end{frame}

\begin{frame}
  \frametitle{Array Representation}

  Consider an alternative approach. Each vertex in our graph keeps track of an
  ID. \pause
  \begin{itemize}
  \item \textsc{Add}: Add a vertex with new ID. \pause
  \item $\textsc{Union}(i, j)$: Update all nodes with same ID as $i$ to have the
    same ID as $j$. \pause
  \item $\textsc{Connected}(i, j)$: Check if $i$ and $j$ have the same ID.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Array Representation Runtime}

  The runtimes end up being: \pause
  \begin{itemize}
  \item \textsc{Add}: $O(1)$ \pause
  \item $\textsc{Union}(i, j)$: $O(V)$. This can be made $O(\log V)$ amortized
    you always merge smaller set to larger set. \pause
  \item $\textsc{Connected}(i, j)$: $O(1)$
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Union Find}

  We want to support two operations: \pause
  \begin{itemize}
  \item $\textsc{Union}(u, v)$: Connect vertices $i$ and $j$. \pause
  \item $\textsc{Find}(u)$: Find the ID representing $i$. \pause
  \end{itemize}
  $\textsc{Connected}$ can be implemented using $\textsc{Find}$.
\end{frame}

\begin{frame}
  \frametitle{Union Find}

  We are going to keep track of each set as a tree. Each vertex $v$ has a
  pointer to its parent $p(v)$. The representative of a set will be the root of
  the tree. \pause

  \begin{itemize}
  \item $\textsc{Find}(u)$: Recursively follow parent pointers until root. \pause
  \item $\textsc{Union}(u, v)$: Set $p(\textsc{Find}(u)) = \textsc{Find}(v)$.
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Union Find Implementation}

  \begin{algorithm}[H]
    \caption{Union Find}
  \begin{algorithmic}[1]
    \Procedure{Find}{$u$}
    \If{$u \not = p(u)$}
    \State \Return $\Call{Find}{p(u)}$
    \Else
    \State \Return $u$
    \EndIf
    \EndProcedure
    \Procedure{Union}{$u$, $v$}
    \State $u \gets \Call{Find}{u}$, $v \gets \Call{Find}{v}$
    \State $p(u) \gets v$
    \EndProcedure
  \end{algorithmic}
  \end{algorithm}

\end{frame}

\begin{frame}
  \frametitle{Union Find Implementation}

  \pause
  \begin{itemize}
  \item $\textsc{Find}(u)$: $O(V)$ \pause
  \item $\textsc{Union}(u, v)$: $O(V)$ \pause
  \end{itemize}

  But this is no better than before?
\end{frame}

\begin{frame}
  \frametitle{Optimizations}

  \begin{block}{Path Compression}
    Update parent pointers as we compute $\textsc{Find}(u)$. We set $p(u) \gets \textsc{Find}(p(u))$.
  \end{block}
  \pause

  \begin{block}{Union by Rank}
    Keep track of $\operatorname{rank}(v)$ which is the rank of the set
    containing $v$, starting at $\operatorname{rank}(v) \gets 0$.

    During $\textsc{Union}(u, v)$, set $p(\textsc{Find}(u)) \gets \textsc{Find}(v)$ if
    $\operatorname{rank}(\textsc{Find}(u)) <
    \operatorname{rank}(\textsc{Find}(v))$. If they are equal, we increment rank
    of the new root.
  \end{block}

\end{frame}

\begin{frame}
  \frametitle{Union Find Implementation}

  \begin{algorithm}[H]
    \caption{Union Find with optimizations}
    \begin{algorithmic}[1]
      \Procedure{Find}{$u$}
      \If{$u \not = p(u)$}
      \State \Return $p(u) \gets l\Call{Find}{p(u)}$
      \Else
      \State \Return $u$
      \EndIf
      \EndProcedure
      \Procedure{Union}{$u$, $v$}
      \State $u \gets \Call{Find}{u}$, $v \gets \Call{Find}{v}$
      \If{$\operatorname{rank}(u) \leq \operatorname{rank}(v)$}
      \State $p(u) \gets v$
      \If{$\operatorname{rank}(u) = \operatorname{rank}(v)$}
      \State $\operatorname{rank}(v) \gets \operatorname{rank}(v) + 1$
      \EndIf
      \Else
      \State $p(v) \gets u$
      \EndIf
      \EndProcedure
    \end{algorithmic}
  \end{algorithm}

\end{frame}

\begin{frame}
  \frametitle{Union Find Runtimes}

  \pause
  \begin{itemize}
  \item $\textsc{Find}(u)$: $O(\alpha(V))$  \pause
  \item $\textsc{Union}(u, v)$: $O(\alpha(V))$ \pause
  \end{itemize}
  $\alpha(V)$ is the inverse Ackermann function. Grows VERY VERY slowly. In fact
  $\alpha(n) \leq 5$ for any remotely feasible input.

  Proof of the runtimes is outside the scope of this lecture though.
  
\end{frame}

\begin{frame}
  \frametitle{Kruskal's Algorithm}

  Want to find minimum spanning tree of a graph.
  \pause
  \begin{block}{Kruskal's Algorithm}
    \begin{itemize}
    \item Sort edges in ascending order.
    \item Repeatedly add shortest edge which connects two unconnected vertices.
    \item Found MST when graph is fully connected.
    \end{itemize}
  \end{block}
  \pause
  Usually implemented using Union-Find. Takes $O(E \log V)$ time.
\end{frame}

\begin{frame}
  \frametitle{Bridge Breaking}

  \begin{block}{Problem}
    Given a set of $N$ islands connected by bridges in a tree. Want to answer $Q$ queries:
    \begin{itemize}
    \item Remove bridge from island $i$ to $j$.
    \item Answer if island $i$ and $j$ are connected.
    \end{itemize}
  \end{block}
  How can we solve this with Union-Find? \pause

  Union-Find doesn't support deletion of edges. However what if we processed the
  queries in reverse order? Then this is exactly the queries $\textsc{Union}(i, j)$ and
  $\textsc{Connected}(i, j)$.

  Total Runtime: $O(Q\alpha(N))$

\end{frame}

\begin{frame}
  \frametitle{Augmenting Information}

  With Union-Find we can store extra information with each set. For example: \pause
  \begin{itemize}
  \item Size of each set. \pause
  \item Number of paths in each set. \pause
  \item Diameter of each tree. \pause
  \end{itemize}

  As long as we can update the operation easily when we union.
\end{frame}

\begin{frame}
  \frametitle{Random Resources}

  This is a past UTPC problem this semester:
  \begin{block}{Problem}
    Given $N$ mines in a tree with the $i$th mine containing $w_i$ resources. Compute the
    expected value of
    \[\max_x w_x - \min_x w_x\]
    where $x$ are on the path from $u$ to $v$ over random $u, v$.
  \end{block}

  Any ideas?
\end{frame}

\begin{frame}
  \frametitle{Random Resources}

  We can separately compute the sum $\max_x w_x$ along all paths $u, v$ and
  the sum $\min_x w_x$ along all paths $u, v$. \pause

  We can compute $\max_x w_x$ by sorting vertices in increasing order by $w_i$.
  Everytime we add a vertex, we add edges incident to already added vertices.
  Want to compute how many new paths are created when we union $u, v$. \pause

  If each set stored $\operatorname{size}(v)$, then this can be computed by
  $\operatorname{size}(u) \cdot \operatorname{size}(v)$. Each path created
  contributes $w_i$ to total value.

\end{frame}

\end{document}
