.DEFAULT_GOAL := all

all:

config:
	git config -l

init:
	git init
	git remote add origin git@gitlab.com:gpdowning/cs104c.git
	git add README.md
	git commit -m 'first commit'
	git push -u origin master

pull:
	git pull
	git status

push:
	git add .gitignore
	git add 00-introduction
	git add 01-shortest-path-i
	git add 02-shortest-path-ii
	git add 03-binary-search
	git add 04-greedy-algorithms
	git add 05-dynamic-programming-i
	git add 06-dynamic-programming-ii
	git add 07-number-theory
	git add 08-segment-trees
	git add 09-max-flow
	git add 10-union-find
	git add 11-string-algorithms
	git add 12-geometry
	git add Makefile
	git add Makefile
	git add README.md
	git commit -m "another commit"
	git push
	git status

status:
	git branch
	git remote -v
	git status
